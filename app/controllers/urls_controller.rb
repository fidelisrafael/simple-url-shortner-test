class UrlsController < ApplicationController
  before_action :set_url, only: [:show]

  # GET /urls/1
  def show
    return redirect_to_root if @url.blank?

    increment_visit_count(@url)

    redirect_to_original_link(@url)
  end

  # POST /urls
  def create
    @url = Url.new(url_params)

    if @url.save
      render json: @url, status: :created, location: @url
    else
      render json: @url.errors, status: :unprocessable_entity
    end
  end

  def popular
    # TODO: pagination
    render json: Url.popular, only: [:short_code, :visits_count, :created_at, :updated_at]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_url
      @url = Url.find_by(short_code: params[:id])
    end

    # Only allow a list of trusted parameters through.
    def url_params
      params.permit(:original_link)
    end

    def increment_visit_count(url)
      UrlVisitCountUpdaterJob.perform_later(url&.id)
    end

    def redirect_to_original_link(url)
      redirect_to url&.original_link
    end
end
