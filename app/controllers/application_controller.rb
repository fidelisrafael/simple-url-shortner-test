class ApplicationController < ActionController::API
  def redirect_to_root
    redirect_to '/'
  end
end
