class Url < ApplicationRecord
  validates :original_link, presence: true, uniqueness: true, format: { with: URI.regexp }

  validates :short_code, presence: true, uniqueness: true

  before_validation :set_defaults, if: :new_record?

  scope :popular, -> { order(visits_count: :desc) }

  SHORT_CODE_BYTES = 3

  def to_param
    short_code
  end

  private

  def set_defaults
    set_short_code
  end

  def set_short_code
    return if persisted?

    self.short_code = SecureRandom.hex(SHORT_CODE_BYTES)
  end
end
