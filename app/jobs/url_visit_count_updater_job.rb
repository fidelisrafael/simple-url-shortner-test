class UrlVisitCountUpdaterJob < ApplicationJob
  queue_as :default

  def perform(url_id)
    Url.increment_counter(:visits_count, url_id)
  end
end
