require "test_helper"

class UrlsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @url = urls(:one)
  end

  test "should create url" do
    assert_difference('Url.count') do
      post urls_url, params: { original_link: "http://google#{SecureRandom.hex(5)}.com.br" }, as: :json
    end

    assert_response 201
  end

  test 'should not create with invalid url' do
    assert_difference('Url.count', 0 ) do
      post urls_url, params: { original_link: SecureRandom.hex }, as: :json
    end

    assert_response 422
  end

  test "should show url" do
    get url_url(@url)

    assert_response :redirect
  end

  test "should redirect to the original url" do
    get url_url(@url)

    assert_redirected_to @url.original_link
  end

  test 'should redirect to home when url doesnt exist' do
    get url_url(SecureRandom.hex)

    assert_redirected_to '/'
  end

  test 'should return the most popular urls' do
    first_url = Url.create(original_link: 'https://www.google.com')
    second_url = Url.create(original_link: 'https://www.google.com?test=1')

    first_url.update_attribute(:visits_count, 10)
    second_url.update_attribute(:visits_count, 100)

    get popular_urls_url

    assert_equal(response.parsed_body[0]['short_code'],  second_url.short_code)
    assert_equal(response.parsed_body[1]['short_code'],  first_url.short_code)

    assert_response 200
  end
end
