Rails.application.routes.draw do
  resources :urls do
    get :popular, on: :collection
  end
end
