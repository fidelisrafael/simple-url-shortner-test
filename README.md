# Simple URL Shortner

---

## Master (Production):

[![pipeline status](https://gitlab.com/fidelisrafael/simple-url-shortner-test/badges/master/pipeline.svg)](https://gitlab.com/fidelisrafael/simple-url-shortner-test/commits/master)

## Develop (Homolog):

[![pipeline status](https://gitlab.com/fidelisrafael/simple-url-shortner-test/badges/development/pipeline.svg)](https://gitlab.com/fidelisrafael/simple-url-shortner-test/commits/development)

# Test Coverage

## Master (Production):

[![coverage report](https://gitlab.com/fidelisrafael/simple-url-shortner-test/badges/master/coverage.svg)](https://gitlab.com/fidelisrafael/simple-url-shortner-test/commits/master)

## Develop (Homolog):

[![coverage report](https://gitlab.com/fidelisrafael/simple-url-shortner-test/badges/development/coverage.svg)](https://gitlab.com/fidelisrafael/simple-url-shortner-test/commits/development)

---

## Sobre

Just a simple url shortner developed on Ruby on Rails 6.

---

## Requirements

- [Ruby >= 2.6.3](https://www.ruby-lang.org/pt/downloads/)
- [SQlite3 >= 3.33](https://www.sqlite.org/download.html)

---

## Stack

- [Rails (6.1.1)](https://guides.rubyonrails.org/v6.1/)
- [Puma (5.1.1)](https://github.com/puma/puma)

[TODO] To be included:

- [Active Model Serializers (0.10.6)](https://github.com/rails-api/active_model_serializers/tree/0-10-stable)
- [Rack CORS (0.4.1)](https://github.com/cyu/rack-cors)
- [Sentry (3.0.0)](https://github.com/getsentry/raven-ruby)
- [Rubocop (0.49.1)](https://github.com/rubocop-hq/rubocop)
- [SimpleCov](https://github.com/colszowka/simplecov)
- [RSpec](https://github.com/rspec/rspec)
- [RSpec Rails](https://github.com/rspec/rspec-rails)
- [FactoryBot](https://github.com/thoughtbot/factory_bot)
- [Webmock](https://github.com/bblimke/webmock)
- [Faker](https://github.com/faker-ruby/faker/)
- [DatabaseCleaner](https://github.com/DatabaseCleaner/database_cleaner)

---

## Development environment setup

Install Ruby 2.4.3 through [`rbenv`](https://github.com/rbenv/rbenv) ou or [RVM](http://rvm.io/).

For this project we will use `rbenv`

```sh
$ git clone git@gitlab.com:fidelisrafael/simple-url-shortner-test.git
$ cd simple-url-shortner-test/
$ rbenv install 2.6.3
$ rbenv local 2.6.3
$ ruby -v  # ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-linux]
$ gem install bundler --no-doc
$ bundle install
```

---

## Database Setup

To setup your local Sqlite3 database run:

```sh
$ bundle exec rails
Created database 'db/development.sqlite3'
Created database 'db/test.sqlite3'
```

And migrate it:

```
$ bundle exec rails db:migrate

== 20210113135635 CreateUrls: migrating =======================================
-- create_table(:urls)
   -> 0.0073s
== 20210113135635 CreateUrls: migrated (0.0074s) ==============================
```

## Serve the HTTP application

After migrating the database, you can serve the local application by running:

```sh
$ bundle exec rails s -b 127.0.0.1
```

To test your setup, try to create a shortned url:

```sh
$  curl -v -H 'Content-Type: application/json' -d "{ \"original_link\": \"https://google.com\" }" http://localhost:3000/urls

< HTTP/1.1 201 Created
< X-Frame-Options: SAMEORIGIN
< X-XSS-Protection: 1; mode=block
< X-Content-Type-Options: nosniff
< X-Download-Options: noopen
< X-Permitted-Cross-Domain-Policies: none
< Referrer-Policy: strict-origin-when-cross-origin
< Location: http://localhost:3000/urls/210ba0
< Content-Type: application/json; charset=utf-8
< Vary: Accept
< ETag: W/"09f6d61d8f9c712e266ae247d0e2e859"
< Cache-Control: max-age=0, private, must-revalidate
< X-Request-Id: 233c9130-fcd3-424d-ad49-f33da374d7d8
< X-Runtime: 0.048548
< Transfer-Encoding: chunked


{"id":1,"original_link":"https://google.com","short_code":"210ba0","visits_count":0,"created_at":"2021-01-13T15:49:33.258Z","updated_at":"2021-01-13T15:49:33.258Z"}
```

---

### URL shortcode algorithm.

I am using [`SecureRandom.hex`](https://ruby-doc.org/stdlib-2.6.3/libdoc/securerandom/rdoc/Random/Formatter.html#method-i-hex) to generate the URL short code with the 3 bytes length (note: The length of the resulting hexadecimal string is twice of the value in `Url::SHORT_CODE_BYTES`)

---

## Deploy

[TODO]

---

## Tests suite

This project uses the default Rails test setup: `ActiveSupport::TestCase` e `MiniTest`, take a look at the Rails Guide: ["Testing Rails Applications"](https://guides.rubyonrails.org/testing.html)

To test the application run:

```sh
$ bundle exec rails test -p

Run options: -p --seed 13006

# Running:

.....

Fabulous run in 0.402329s, 12.4277 runs/s, 19.8842 assertions/s.
5 runs, 8 assertions, 0 failures, 0 errors, 0 skips
```

---

## Code Coverage

[TODO: Include Simplecov]

---

## F.A.Q

---

## Useful References

- https://gitlab.com/fidelisrafael/simple-url-shortner-test

- https://guides.rubyonrails.org/

- https://guides.rubyonrails.org/testing.html

- https://github.com/rspec/rspec

- https://github.com/thoughtbot/factory_bot

- https://github.com/bblimke/webmock

- https://github.com/faker-ruby/faker/

- https://github.com/DatabaseCleaner/database_cleaner
