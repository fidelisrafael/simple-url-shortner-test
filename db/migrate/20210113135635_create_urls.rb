class CreateUrls < ActiveRecord::Migration[6.1]
  def change
    create_table :urls do |t|
      t.string :original_link, unique: true, null: false
      t.string :short_code, unique: true, null: false
      t.integer :visits_count, index: true, default: 0

      t.timestamps
    end
  end
end
